<html lang="en">
<head>

            <meta charset="utf-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
            <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
            <meta name="viewport" content="width=device-width" />
            <title>FreshAir </title>
            <link href="css/main.css" rel="stylesheet" />
            <link rel="stylesheet" href="css/bootstrap.css">

            <!--     Fonts     -->
            <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">



            <link href="https://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
            <link href='https://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>
            <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat:400,700,900">
            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">


            <link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="images/apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="images/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="images/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="images/apple-touch-icon-152x152.png" />
<link rel="icon" type="image/png" href="images/favicon-196x196.png" sizes="196x196" />
<link rel="icon" type="image/png" href="images/favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="images/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="images/favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="images/favicon-128.png" sizes="128x128" />
<meta name="application-name" content="&nbsp;"/>
<meta name="msapplication-TileColor" content="#FFFFFF" />
<meta name="msapplication-TileImage" content="mstile-144x144.png" />
<meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
<meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
<meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
<meta name="msapplication-square310x310logo" content="mstile-310x310.png" />

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FreshAir</title>
</head>
<body class="main"  data-spy="scroll" data-target="#scroll-spy">
    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-home" href="#home" role="button">
                    <svg class="lnr lnr-home"><use xlink:href="#lnr-home"></use></svg>
                </a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#scroll-spy" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<i class="material-icons">&#xE5D2;</i>
				</button>
                <!-- 				<a class="navbar-brand" href="#home">Boom Scroll
					<i class="material-icons">mouse</i>
				</a> -->
            </div>

            <!-- Collect the nav links, forms for toggling -->
            <div class="collapse navbar-collapse" id="scroll-spy">

                <!-- navbar-nav -->
                <ul class="nav navbar-nav ">
                    <li><a href="#about">About</a></li>
                    <li><a href="#demo">Demo</a></li>

                </ul>
                <!-- /.navbar-nav -->
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <!-- /.fixed navigation -->
    <section id="home">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>FreshAir</h1>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
                    <p>Sign up for updates and to learn about joining our beta user group.</p>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-xs-12">
                    <form class="form-inline" role="form" action="process.php" method="POST" id="main">
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputEmail2">Email address</label>
                            <input type="email" id="form-email" class="form-control transparent" placeholder="Your email here">
                        </div>
                        <button type="submit" name="submit" class="btn btn-danger btn-fill">Notify Me</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- /#home -->

    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section-header">
                        About
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
                    <p>FreshAir is a new community for outdoor enthusiasts. The platform will connect private landowners with recreationists seeking to get outdoors who may not have the land or connections to do so.</p>


                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /#about -->

    <section id="demo" class="test">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section-header">
                        Demo
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="video-roll--container col-xs-12">
                    <ul class="video-roll">
                        <li>
                            <a href="demo/index.html" target="_blank"></a>
                        </li>
                        <li>
                            <a href="demo/index-2.html" target="_blank"></a>
                        </li>
                        <li>
                            <a href="demo/index-4.html" target="_blank"></a>
                        </li>
                    </ul>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <!--
            <div class="row">
                <div class="col-xs-12 text-center">
                    <a class="tutorials-link" href="https://www.youtube.com/channel/UCydykDsTWRIVnxKHW3SHPQA" role="button">Watch Videos</a>
                    <h4>Check out my YouTube tutorials</h4>
                </div>
                <!-- /.col -->
        </div>


        <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>



    <footer>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="footerLinks">
                        <ul>
                            <li id="space">
                                <a href="https://www.facebook.com/FreshAir-390602511351444/?ref=br_rs">
                                    <i class="fa fa-facebook-square"></i>
                                </a>
                            </li>
                            <li id="space">
                                <a href="https://twitter.com/FreshAir_LLC">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li id="space">
                                <a href="https://www.instagram.com/freshair_llc/ ">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                            <li id="space">
                                <a href="mailto:omar.shaarawi@freshair.us?&subject=Inquiry ">
                                    <i class="fa fa-envelope-o"></i>
                                </a>
                            </li>

                        </ul>
                    </div>
                    <div class="copyright">
                        <p>&copy; Copyright 2018, FreshAir LLC</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <a href="#home" role="button" class="scrollTop">
        <svg class="lnr lnr-chevron-up"><use xlink:href="#lnr-chevron-up"></use></svg>
    </a>

</body>
   <script src="js/jquery-1.10.2.js" type="text/javascript"></script>
   <script src="js/main.js" type="text/javascript"></script>
<script src="https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js"></script>
   <script src="js/bootstrap.min.js" type="text/javascript"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.js"></script>
      <script src="js/main2.js"></script>
   <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/19e5e87c9a4d01c3d00b6c1e0/cf475a8feff91974f30b5d8ab.js");</script>
</html>
